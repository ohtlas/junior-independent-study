﻿using System.Collections;
using UnityEngine;

public class Teleport : MonoBehaviour
{
    public GameObject ui;
    public GameObject objToTP;
    public Transform Destination;
    private bool showGUI = false;


    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            showGUI = true;
        }
        else if ((col.gameObject.tag == "Player") && Input.GetKeyDown(KeyCode.E))
        {
            objToTP.transform.position = Destination.transform.position;
        }
    }
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            showGUI = false;
        }
    }
    void OnGUI()
    {
        if (showGUI == true)
        {
            ui.SetActive(true);
        }
    }
}

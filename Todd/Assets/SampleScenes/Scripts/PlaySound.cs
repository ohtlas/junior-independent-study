﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RealSpace3D;

public class PlaySound : MonoBehaviour {

    public AudioClip SoundToPlay;
    RealSpace3D_AudioSource audio;

	// Use this for initialization
	void Start () {
        audio = GetComponent<RealSpace3D_AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
	}
    void OnTriggerStay(Collider col)
    {
        if ((col.gameObject.tag == "Player") && Input.GetKeyDown(KeyCode.E)){
            audio.rs3d_PlaySound();
        }
    }
}

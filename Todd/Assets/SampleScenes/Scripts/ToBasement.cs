﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToBasement : MonoBehaviour {

    public GameObject player;
    public Transform DestinationUnderground;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            player = col.transform.gameObject;
            player.transform.position = DestinationUnderground.transform.position;
            player.transform.rotation = DestinationUnderground.transform.rotation;
        }

    }
}

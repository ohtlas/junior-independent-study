﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RealSpace3D;

public class FrontDoorInteract : MonoBehaviour {



    private RealSpace3D_AudioSource AudioSource;


    // Use this for initialization
    void Start () {
         AudioSource = gameObject.GetComponent<RealSpace3D_AudioSource>();
        AudioSource.rs3d_LoopSound(true);
    }

    public void playSound()
    {
        AudioSource.rs3d_MuteSound(false);
        AudioSource.rs3d_PlaySound();
        AudioSource.rs3d_LoopSound(true);
    }
    public void stopSound()
    {
        AudioSource.rs3d_MuteSound(true);
        AudioSource.rs3d_LoopSound(false);

    }

    // Update is called once per frame

    }
}
